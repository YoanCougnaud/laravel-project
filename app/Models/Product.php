<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    // protected $fillable = [
    //     'nameProduct',
    //     'slug',
    //     'description',
    //     'price'
    // ];

    protected $fillable = [
        'category',
        'capacityQuantity',
        'amount'
    ];
}
