<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        
        $products = Product::all();

        return view('api.product', compact('products'));

    }

    /**
     * Display a listing of the resource.
     *
     * @param  str  $type
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {

        // $client = new GuzzleHttp\Client();
        
        // $response = $client->get('https://apis.fr.corp.leroymerlin.com/api-product/v2/nomenclatures/web');
        
        $client = new Client();

        $res = $client->request('GET', 'https://api-gateway.leroymerlin.fr/api-product/v2/nomenclatures/'.$type, [
            'headers' => [ 
                'X-Gateway-APIKey' => 'vx64AonXBZVIIDkvhZHskyQLEN15iLk2',
            ]   
        ]);
        
        return json_decode($res->getBody());
        
        // return Product::limit(10)->get();
        // return $response->json();
        
        // return Product::all();
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nameProduct' => 'required',
            'slug' => 'required',
            'price' => 'required'
        ]);

        return Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = new Client();

        $res = $client->request('GET', 'https://api-gateway.leroymerlin.fr/api-product/v2/products/'.$id, [
            'headers' => [ 
                'X-Gateway-APIKey' => 'vx64AonXBZVIIDkvhZHskyQLEN15iLk2',
            ],
        ]);
        
        //réponse en objet
        $stream = json_decode($res->getBody());

        //$stream->id;
        //$stream->category;
        //$stream->visibleFees[0]->amount;

        return $stream->visibleFees[0]->amount;

        // //réponse en tableau 
        // $stream = json_decode($res->getBody(), true);
        // return $stream['id'];

        // return $stream;

        // return Product::find($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showTypeId($type,$id)
    {
        $client = new Client();

        $res = $client->request('GET', 'https://api-gateway.leroymerlin.fr/api-product/v2/nomenclatures/'.$type.'/'.$id.'/products', [
            'headers' => [ 
                'X-Gateway-APIKey' => 'vx64AonXBZVIIDkvhZHskyQLEN15iLk2',
            ],
            'limit' => 10, 
        ]);
        
        $leroyResponses = json_decode($res->getBody());

        $leroyResponses = $leroyResponses->data;

        foreach( $leroyResponses as  $leroyResponse){
            $products = new Product;
            $products->category = $leroyResponse->category;
            $products->capacityQuantity = $leroyResponse->capacityQuantity;
            // $products->amount = $leroyResponse->visibleFees[0]->amount;
            $products->save();
        }
        return "DONE";

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showImage($id)
    {
        $client = new Client();

        $res = $client->request('GET', 'https://api-gateway.leroymerlin.fr/api-product/v2/products/'.$id.'/media', [
            'headers' => [ 
                'X-Gateway-APIKey' => 'vx64AonXBZVIIDkvhZHskyQLEN15iLk2',
            ]   
        ]);
        
        return json_decode($res->getBody());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCharacteristics($id)
    {
        $client = new Client();

        $res = $client->request('GET', 'https://api-gateway.leroymerlin.fr/api-product/v2/products/'.$id.'/characteristics', [
            'headers' => [ 
                'X-Gateway-APIKey' => 'vx64AonXBZVIIDkvhZHskyQLEN15iLk2',
            ]   
        ]);
        
        return json_decode($res->getBody());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Product = Product::find($id);
        $Product->update($request->all());
        return $Product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Product::destroy($id);
    }

    /**
     * Search for a name.
     *
     * @param  str  $nameProduct
     * @return \Illuminate\Http\Response
     */
    public function search($nameProduct)
    {
        return Product::where('nameProduct', 'like', '%'.$nameProduct.'%')->get();
    }
}
