<?php
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::resource('produits', ProductController::class);

// Route::get('/produits',function(){
//     // return ProductController::limit(10)->get();
//     return ProductController::all();
// });


//route public
Route::get('/produits', [ProductController::class, 'test']);
Route::get('/produits/{id}', [ProductController::class, 'show']);
Route::get('/produits/image/{id}', [ProductController::class, 'showImage']);
Route::get('/produits/characteristiques/{id}', [ProductController::class, 'showCharacteristics']);
Route::get('/produits/nomenclatures/{type}', [ProductController::class, 'index']);
Route::get('/produits/search/{nameProduct}', [ProductController::class, 'search']);
Route::get('/produits/nomenclatures/{type}/{id}', [ProductController::class, 'showTypeId']); 
    
//routes privés
Route::group(['middleware'=> ['auth:sanctum']], function(){
        
    Route::post('/produits', [ProductController::class, 'store']);
    Route::put('/produits/{id}', [ProductController::class, 'update']);
    Route::delete('/produits/{id}', [ProductController::class, 'destroy']);
        
});

//Authentification d'un utilisateur
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
