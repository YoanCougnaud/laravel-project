<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('products', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('nameProduct');
        //     $table->string('slug');
        //     $table->string('description')->nullable();
        //     $table->decimal('price', 5, 2);
        //     $table->timestamps();
        // });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category');
            $table->integer('capacityQuantity');
            $table->decimal('amount', 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
